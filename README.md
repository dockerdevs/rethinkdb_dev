# Dockerized Development Environment for Contribution to RethinkDB #

This repository provides a ready-made docker image with all needed dependencies for getting up and running with rethinkdb development.
The project is in no way associated with official rethinkdb which is hosted on [Github - RethinkDB](https://github.com/rethinkdb/rethinkdb).

### What is this repository for? ###

* A very up-to-date development environment container for rethinkdb development
* 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

I develop and contribute to a lot of repos and do not want to get the development environment pollute one setup to another. One of the best ways
to do so is to use docker containers with very specific dev environments for specific development.

Always read the contribution guidelines before creating a pull request. Read the documentation on the actual dev projects. This image will only
help with setting up the initial dev environment which can then be used seamlessly.

Always keep your /etc/resolve.conf updated with proper nameservers if you are not able to access the internet from inside the container.

Contributions are always welcome. ENJOY!

Follow me on Twitter @wolverine2k

### How do I get set up? ###

* Summary of set up
# Clone the rethinkdb repo from [RethinkDB](https://github.com/rethinkdb/rethinkdb)
# Pull the image from wolverine2k/rethinkDB_dev
# Run the image using `docker run -dt -v <cloned_repo>:/rethinkdb wolverine2k/rethinkDB_dev`
# Access the build shell using `docker exec -i -t <CONTAINER_IMAGE_ID>`
# A simple script has been created for easier startup in this repo called `runRethinkDev.sh`
  Use that instead if you want to ease out on starting up the image

* Configuration
# None as of today

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
