#!/bin/bash

RED='\033[0;31m'
NC='\033[0m' # No Color

function print_help {
  echo -e "${RED}"
  echo -e "Usage:  $0 <rethinkdb_source_absolute_path>"
  echo
  echo -e "\tRuns the development container for RethinkDB Development"
  echo -e "\tPlease provide the path to the directory where you have cloned"
  echo -e "\trethinkdb source code."
  echo -e "${NC}"
}

if [ -z "$1" ]; then
    print_help
    exit 1
fi

RETHINKDB_SOURCE_PATH=$1

if [ "$1" = "." ]; then
  RETHINKDB_SOURCE_PATH=`pwd`
fi

docker run -dt -v $1:/rethinkdb wolverine2k/rethinkDB_dev
sleep 1
CONTAINER_IMAGE=`docker ps | grep -E 'wolverine2k/rethinkDB_dev' | awk '{print $1}'`
docker exec -i -t ${CONTAINER_IMAGE}

#docker images | grep -E '*temp*' | awk -e '{print $3}'
# sudo docker run -dt -v /home/naresh/projects/github/rethinkdb:/rethinkdb temp
# sudo docker exec -i -t 416738a2576d
